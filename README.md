# ElixirGenstage

Experiments with / learning of Elixir GenStage module.

Follows the instructional material laid out at
https://elixirschool.com/en/lessons/advanced/gen-stage/ .
