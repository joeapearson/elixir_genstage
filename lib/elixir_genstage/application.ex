defmodule ElixirGenstage.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Starts a worker by calling: ElixirGenstage.Worker.start_link(arg)
      # {ElixirGenstage.Worker, arg}
      {ElixirGenstage.Producer, 0},
      {ElixirGenstage.ProducerConsumer, []},
      %{
        id: 1,
        start: {ElixirGenstage.Consumer, :start_link, [[]]}
      },
      %{
        id: 2,
        start: {ElixirGenstage.Consumer, :start_link, [[]]}
      },
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: ElixirGenstage.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
